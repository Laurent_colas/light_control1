# Light_control
Flask application for controlling lights in my house. 
This includes ESP8266 and ESP32 firmware for motion detectors, On-Off switches, On-Off Buttons. It also includes flask application to physically control light state / brightness with a 16 channel pwm controller, create macros to bundle rooms together and control these rooms with a simple click and check state online of every light. 
